# About Git

Git is a software revision control system. The primary role of revision control in software developemnt is to organize and reconcile different versions that are created when multiple users collaborate on a project. Git stores the project in a repository, commonly called a _repo_.

Unlike earlier revision control systems like Subversion, Git is _decentralized_. That means that each developer begins with a copy of the entire project, not just the part they intend to edit, and they are able to work on their copy indefinitely before contributing their changes back to the main repository (the _origin_). There may not even be a main repository - a small project developed by a single person may only exist on their computer<sup>1</sup>. Even in this simple case, Git is an invaluable tool to manage the project's features, versions and history.

Many developers prefer to interact with Git through the command line interface (CLI) because it's quick and easy once you memorize the basic commands. However, there are many graphical interfaces that can make Git easier to work with if you prefer to avoid the command line.

<sup>1</sup> <small>I don't recommend keeping the only copy of your project on your computer. Git hosts like Github and Bitbucket don't just provide a central hub for collaboration, they also make great backup systems. You should _always_ have a backup.

# Initializing a Git Repository

To create a new repository, navigate to your project directory and run `git init`:

    $ cd ~/Projects/MyApp
    $ git init
    Initialized empty Git repository in ~/Projects/MyApp/.git/
    
That's it. You have a Git repository.

# Ignoring Files

There may be some files that you want to permanently exclude from your repositiory. Examples of these are certain system files, like the `.DS_Store` files created by OS X Finder; generated code, like Python `.pyc` files; and IDE configurations, like Xcode `xcuserdata` files. You can tell git to ignore these files by listing them in a file called `.gitignore` at the root level of your project. Here is a sample `.gitignore` file that illustrates different kinds of  (lines beginning with `#` are comments):

    # Ignore a system file:
    .DS_Store
    
    # Ignore a specific file by name:
    IgnoredFile.txt
    
    # Ignore a directory:
    MyDir
    
    # Ignore files by type:
    *.pyc

# Adding & Committing Changes

Once you have completed some work, you can _commit_ your changes to your local copy of the repo. First, run `git status` to view the state of your branch:

    $ git status
    
Depending on the work you have done, you may see files listed in any of three different categories: 

1. **Changes to be committed:** files that have been added or changed and are ready to commit.
2. **Changes not staged for commit:** files that have been changed but have not been _staged_, or added to the commit. If you commit without adding these files first, they won't by included (but you won't lose your changes and you can still add them to future commits).
3. **Untracked files:** files that have been added to the project but aren't being tracked by Git. They will need to be added to the repo before you can commit them<sup>2</sup>.

<sup>2</sup> If you have untracked files that you don't want to commit, consider adding them to your project's `.gitignore` file.

## Staging Files

You can stage files to be committed by running `git add`:

    # Add a single file:
    $ git add MyFile.txt
    
    # Add a directory, including all files within it:
    $ git add MyDir
    
    # Add all files of a type:
    $ git add *.css
    
## Unstaging Files

You can also _unstage_ files. This can be useful if you accidentally added a file that you wanted to leave out of your commit. If you simply want to unstage a file without reverting your changes, use `git reset`:

    $ git reset HEAD MyFile.txt
    
This leaves your changes intact on your file system, but resets the state of the file _in the repository_ to the state of the previous commit. This is usually what you'll want to do. 

You can also completely remove a file from the repository with `git rm --cached`:

    $ git rm --cached MyFile.txt
    
This removes the file from your Git repo but doesn't delete the file from your file system. If you run `git status` after running `git rm --cached` on a file, you'll see the file listed under _Untracked files_.

## Commiting Files

Once you have staged your files, run `git commit` to commit your changes:

    $ git commit -m "Describe your changes."
    
The `-m` flag is used to set a message for your commit. Be descriptive in your commit messages. For example, if you are committing bugfixes, "bugfixes" is not an adequate commit message. Keep the message short but explain what you fixed. If you use a project management tool to track tickets, you might want to include the ticket number, too. Your development and project managers will thank you (or at least quietly appreciate the effort).

## Viewing Commit Logs

You can view the commit history by running `git log`:

    $ git log
    commit 6314b013700f82aefec88b8e91048eeeb6aeb0ae
    Author: Your Name <you@company.tld>
    Date:   Mon Mar 7 20:04:18 2016 -0500
    
The log is a good, quick way to review the recent history of your project.

# Branches

Git uses _branches_ to manage different versions of a project in parallel. A new repo will contain a single branch called `master`. Although you _can_ make changes to `master`, you generally won't (or shouldn't). It is better practice to create a branch for each new feature.

## Listing Branches

You can list your local branches by simply running `git branch`:

    $ git branch
    * master
    * feature-1
    * feature-2
    
If you also want to list remote branches, add the `--all` flag:

    $ git branch --all
    
## Changing Branches

The `checkout` command changes the current branch. If you are currently on the `master` branch and you want to switch to the `feature-1` branch (assuming it exists), run `git checkout`:

    $ git checkout feature-1
    
Run `git checkout` again to switch back to the `master` branch:

    $ git checkout master

## Creating a Branch

Creating a new branch is similar to checking out an existing one - just add the `-b` flag to the `git checkout` command:

    $ git checkout -b feature-3
    Switched to a new branch 'feature-3'

# Merging & Conflicts

The changes you commit only exist on your current branch. _Merging_ is the process of combining changes from multiple branches into a single branch. A simple merge workflow looks like this:

    # Checkout your source branch:
    $ git checkout master

    # Checkout a new branch for your feature:
    $ git checkout -b feature
    
    # Do work, stage your changes and commit...
    
    # Checkout your source branch again:
    $ git checkout master
    
    # Merge your feature branch into your source:
    $ git merge feature
    
The `git merge` command merges the branch you specify (`feature` in the above example) into your current branch. Run `git log` after merging and you should see the combined history of both branches.

## Merge Conflicts

While Git is generally great at figuring out how to combine branches, merges don't always go smoothly. Conflicts often arise when the same lines of code were changed in two or more branches. Consider the following scenario:

Two developers have been assigned to work on two different features. Both features involve making changes to the same file, some of which affect the same lines of code. Once both features are complete, the project lead will merge them into `master`:

    $ git checkout master
    $ git merge feature-1
    Updating 6314b01..2ba8d5d
    Fast-forward
      file.txt | 2 +-
    1 file changed, 1 insertion(+), 1 deletion(-)

The first feature merged easily. The output of `git merge` shows a summary oof the changes made to the file. Now for the second feature:

    $ git merge feature-2
    Auto-merging file.txt
    CONFLICT (content): Merge conflict in file.txt
    Automatic merge failed; fix conflicts and then commit the result.
    
The merge failed because `feature-2` contains changes that conflict with `feature-1`. The output shows that the conflict is in `file.txt`. 

So how do we fix it? Open the affected file and you'll find that Git has marked up the conflicting lines for you:

    <<<<<<< HEAD
    Previously merged code from feature 1
    =======
    Conflicting code from feature 2
    >>>>>>> feature-2
    
The code between `<<<<<<< HEAD` and `=======` was previously merged from `feature-1`. The code between `=======` and `>>>>>>> feature-2` is the code that you're trying to merge, which conflicts with the previously merged code.

Resolving the conflict is a simple three step process:

1. Choose which version of the code to keep or rewrite the conflicting code as necessary to preserve any important changes from both versions.

2. Delete the Git markup:
    * `<<<<<<< HEAD`
    * `=======`
    * `>>>>>>> feature-2`

3. Stage and commit to complete the merge.

Finally, it's always a good idea to run `git status` to make sure your working directory is clean and `git log` to make sure the history is correct.

# Remote Repositories

So far, we have only worked with your local repository because that's where you'll do all your work. If you want to collaborate with other developers, you'll need a _remote_ repository. The remote resides on a server, usually (but not always) maintained by a commercial Git host. There are many commercial hosts but he most popular are [Github](https://github.com) and [Bitbucket](https://bitbucket.com). Aside from some user interface and pricing differences they are very similar.

## Creating a Remote Repository

Your host will provide an interface to create a new remote repository. Each repository will have a unique URL that you can use to interact with the remote. 

## Adding a Remote to a Project

If you have an existing project, run `git remote add <repo url>` to give your local repo a reference to the remote:

    $ git remote add git@host.com/my-repo
    
You can then push your entire local repo to the remote with one command:

    $ git push -u origin --all

## Cloning and Forking

_Cloning_ a remote repository creates a _local_ copy of it, meaning a copy that resides on your computer:

    $ git clone git@host.com/my-repo

The clone will automatically be given a reference to the remote so you can pull and push changes.

_Forking_ a remote repository creates a _remote_ copy of it, meaning a copy that resides with your Git host. Forking gives you ownership and administrative control of your copy. You can clone your fork to obtain a local copy to work with.

You don't need to fork every repository you want to work with. In general, it makes sense to fork if you don't have write access to the repo _and_ you want to contribute to it. You can't push changes to a repo unless you have write access, so a simple clone won't be of much use in this case. (It should go without saying that no sane repo owner gives write access to just anybody.)

If you have write access to the repo, or you just want to use the code and don't plan on contributing, you should be fine with a clone.

## Fetching

Your local copy will sometimes get out of sync with the remote as other developers push changes. Use `git fetch` to bring your local copy up to date with the latest changes from your remote:

    # Fetch from origin:
    $ git fetch origin
    
    # Fetch everything (more common):
    $ git fetch --all

## Pulling and Pushing Changes

You can obtain the latest changes for a specific branch by _pulling_ them from the remote:

    $ git pull origin <branch>
    
Likewise, you can contribute your changes to the remote repo by _pushing_ them:

    $ git push origin <branch>
    
You should always pull before working on a branch to make sure that you're starting with the latest code. You should also and pull before you push your changes to make sure your local branch is still up to date. If your local branch is _behind_ the remote, the remote will refuse your pushes until you bring it up to date. Pulling remote changes over top of your local changes will occasionally result in merge conflicts. If that happens, resolve the conflicts then commit and push.

To summarize, here are a few typical workflows that involve remote repositories:

1. Starting a new branch:

        # Clone the repo and cd into it:
        $ git clone git@host.com/my-repo
        $ cd my-repo
    
        # Create a new branch for your work:
        $ git checkout -b my-feature
    
        # Work, stage and commit...
    
        # Push your changes to the remote:
        $ git push origin my-feature
        
2. Working on an existing branch:

        # Checkout your branch:
        $ git checkout my-feature
        
        # Pull the latest changes from the remote:
        $ git pull origin my-feature
    
        # Work, stage and commit...
        
        # Make sure your branch is still up to date with the remote:
        $ git pull origin my-feature
    
        # Push your changes to the remote:
        $ git push origin my-feature

## Pull Requests

Once you have finished a feature, you'll want to notify your project lead that it's finished and ready for review, and hopefully ready to be merged into the main branch. You typically do this by submitting a _pull request_. Your Git host should provide an interface that will let you select the branch you want to merge and the branch it should be merged into.

## Authentication

Your Git host will require that you identify yourself with each request. There are two ways to authenticate your identity: username and password, or [SSH (Secure Shell)](https://en.wikipedia.org/wiki/Secure_Shell).

### Username / Password

Username and password authentication is usually only used to interact with remote repositories over HTTPS. If you use this form of authentication you will have to provide your username and password with every request. Needless to say, that will quickly become annoying.

### SSH

SSH is the preferred means of authentication for Git. The initial setup is more complicated than choosing a username and password but the transparency and security that SSH provides is well worth the effort:

1. Create your SSH keypair [as described here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html). You will create a public key and a private key. You should keep a secure backup of both and _never_ disclose your private key to anyone.
2. Register your public key with your Git host.
3. Make sure your remotes are configured to use SSH rather than HTTPS. You can check the current URL by running `git remote -v`. If you need to update it, run `git remote set-url origin <new url>`.
4. Interact with your remote repository normally and enjoy never having to use a password.